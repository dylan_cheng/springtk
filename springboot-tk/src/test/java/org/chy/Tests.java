package org.chy;

import java.io.File;

import org.chy.utils.Constants;
import org.chy.utils.QRCodeUtils;
import org.junit.Test;

public class Tests {

	@Test
	public void contextLoads() {
		QRCodeUtils qrCodeUtils = new QRCodeUtils(300, 300);
		qrCodeUtils.setMargin(1);
		qrCodeUtils.createQrImage("https://www.baidu.com/", Constants.PATH + "dsd.png");
	}

	// main方法测试工具类
	public static void main(String[] args) {
		QRCodeUtils qrCode = new QRCodeUtils(300, 300);
		// 设置二维码的边缘为1
		qrCode.setMargin(1);
		// 设置输出到桌面
		String path = System.getProperty("user.home") + File.separator + "Desktop" + File.separator + "test.png";
		// 创建图片二维码
		qrCode.createQrImage("https://www.baidu.com/", path);
		// 识别图片二维码的内容
		System.out.println(qrCode.decodeQrImage(new File(path)));// https://www.baidu.com/
	}

}
