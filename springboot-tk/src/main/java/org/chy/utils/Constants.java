package org.chy.utils;

import java.io.File;

/**
 * 常量接口
 * @author  Chenghy
 * @date    2018年01月02日  02时16分33秒
 * @version 1.0
 */
public interface Constants {
	
	/**
	 * 登录对象
	 */
	public static final String LOGING = "login";
	
	/**
	 * 二维码路径
	 */
	public static final String PATH =  System.getProperty("user.dir") + File.separator + "src"
						+ File.separator + "main" + File.separator 
						+ "resources" + File.separator + "images" + File.separator ;
}
