package org.chy.utils;

import java.util.UUID;

/**
 * @author  Chenghy
 * @date    2017年12月21日  01时23分55秒
 * @version 1.0
 */
public class UUIDTool {
	
	public UUIDTool() {}
	
	public static String getUUID() {
		//生成uuid
		String uuid = UUID.randomUUID().toString();
		//去掉“-”
		uuid = uuid.replace("-", "");
		return uuid;
	}
}
