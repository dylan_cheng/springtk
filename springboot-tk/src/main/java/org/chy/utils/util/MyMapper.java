package org.chy.utils.util;

import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

/**
 * @author  Chenghy
 * @date    2017年12月20日  11时20分32秒
 * @version 1.0
 */
public interface MyMapper<T> extends Mapper<T>,MySqlMapper<T>{

	//特别注意,该接口不能被扫描到,否则会出错的.
	
}
