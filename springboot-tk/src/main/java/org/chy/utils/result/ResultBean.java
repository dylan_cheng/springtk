package org.chy.utils.result;

import java.io.Serializable;

import lombok.Data;

/**
 * @author  Chenghy
 * @date    2017年12月21日  12时27分37秒
 * @version 1.0
 */
@Data
public class ResultBean<T> implements Serializable {

	private static final long serialVersionUID = -5016422366554199536L;
	
	/*成功*/
	public static final int SUCCESS = 200;
	
	/*服务器内部错误*/
	public static final int FAIL = 500;
	
	private String msg = "success";
	
	private int code = SUCCESS;
	
	private T data;
	
	public ResultBean() {
		super();
	}
	
	public ResultBean(T data) {
		super();
		this.data = data;
	}
	
	public ResultBean(Throwable e) {
		super();
		this.msg = e.toString();
		this.code = FAIL;
	}
	
	public String getMsg() {
		return msg;
	}
	
	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	public int getCode() {
		return code;
	}
	
	public void setCode(int code) {
		this.code = code;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}
}