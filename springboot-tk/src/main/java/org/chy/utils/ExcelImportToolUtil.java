package org.chy.utils;

import java.io.InputStream;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.entity.result.ExcelImportResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;


public class ExcelImportToolUtil {

	private static final Logger log = LoggerFactory.getLogger(ExcelImportToolUtil.class);

	/**
	 * 导入excel文件
	 * @param file			上传的文件
	 * @param params		导入参数设置,可设置参数-->
	 *  titleRows 表格标题行数 默认0,headRows 表头行数 默认1,startRows 字段真正值和列标题之间的距离 默认0,
	 * 	needSave 是否需要保存上传的Excel,needVerfiy 是否需要校验上传的Excel,
	 * 	saveUrl 默认"upload/excelUpload",importFields 导入时校验数据模板,是不是正确的Excel
	 * @param pojoClass		 
	 * @return
	 * @throws Exception
	 */
	public static <T> ExcelImportResult<T> excelImportPreserved(MultipartFile file,Class<?> pojoClass,ImportParams params)
			throws Exception {

		ExcelImportResult<T> list=new ExcelImportResult<>();
		InputStream input = file.getInputStream();
		
		list = ExcelImportUtil.importExcelVerify(input, pojoClass, params);

		log.debug(list.toString());
		return list;
	}
	
}
