package org.chy.utils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
//import java.lang.reflect.InvocationTargetException;
//import java.lang.reflect.Method;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Workbook;
import org.jeecgframework.poi.excel.ExcelExportUtil;
import org.jeecgframework.poi.excel.entity.ExportParams;

/**
 * 导出Excel工具类
 * @author 
 * @date 2017年12月15日 17时13分26秒
 * version 1.0
 */
public class ExportExcelUtil {
	
	/**
	 * 
	* @Title: downLoadExecl 
	* @Description: TODO(基于注解方式xls格式文件导出) 
	* @param request
	* @param response
	* @param titleName 标题	
	* @param list    实体集合
	* @return void    返回类型 
	* @throws
	 */
	public static void downLoadExecl(HttpServletRequest request,HttpServletResponse response,String titleName,List<?> list){
	    response.setHeader("content-Type", "application/vnd.ms-excel");
	    // 下载文件的默认名称
	    try {
			response.setHeader("Content-Disposition", "attachment;filename=" + java.net.URLEncoder.encode(titleName,"UTF-8")+ ".xls");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	    //获取list中？类型
	    Class<?> pojoClass=list.get(0).getClass();
	   /* for(int i=0;i<list.size();i++){
	    	Method setOrderNum;
			try {
				setOrderNum = list.get(i).getClass().getMethod("setOrderNum", Integer.class);
				setOrderNum.invoke(list.get(i),i+1);
			} catch (NoSuchMethodException | SecurityException|IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				e.printStackTrace();
			}
	    	
	    }*/
	    response.setCharacterEncoding("UTF-8");
	    ExportParams exportParams=new ExportParams();
	    exportParams.setTitleHeight((short)20);
	    exportParams.setTitle(titleName);
	    Workbook workbook = ExcelExportUtil.exportExcel(exportParams,pojoClass,list);
	    try {
			workbook.write(response.getOutputStream());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
