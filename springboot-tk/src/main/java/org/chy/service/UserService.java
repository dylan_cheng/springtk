package org.chy.service;

import java.util.List;

import org.chy.entity.User;

/**
 * @author  chenghy
 * @date    2018年01月07日 13时12分20秒
 * @Description 发上等愿,结中等缘,享下等福;向高处立,就平处坐,从宽处行.
 * @Package org.chy.service
 */
public interface UserService {
	
	List<User> queryUserList();
}
