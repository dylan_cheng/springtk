package org.chy.service.impl;

import java.util.List;

import org.chy.entity.User;
import org.chy.mapper.UserMapper;
import org.chy.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author  chenghy
 * @date    2018年01月07日 13时12分29秒
 * @Description 发上等愿,结中等缘,享下等福;向高处立,就平处坐,从宽处行.
 * @Package org.chy.service.impl
 */
@Service("userService")
public class UserServiceImpl implements UserService {

	private @Autowired UserMapper userMapper;
	
	@Override
	public List<User> queryUserList() {
		return userMapper.selectAll();
	}

}
