package org.chy.mapper;

import org.chy.entity.User;
import org.chy.utils.util.MyMapper;

/**
 * @author  chenghy
 * @date    2018年01月07日 12时35分33秒
 * @Description 发上等愿,结中等缘,享下等福;向高处立,就平处坐,从宽处行.
 * @Package org.chy.mapper
 */
public interface UserMapper extends MyMapper<User>{

}
