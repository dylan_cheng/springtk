package org.chy.entity;

import javax.persistence.Table;

import org.apache.ibatis.type.Alias;

/**
 * @author  chenghy
 * @date    2018年01月07日 12时32分36秒
 * @Description 发上等愿,结中等缘,享下等福;向高处立,就平处坐,从宽处行.
 * @Package org.chy.entity
 */
@Alias("User")
@Table(name="sys_user")
public class User extends BaseEntity  {

	private static final long serialVersionUID = 3048421986655597567L;

	private String username;
	
	private String password;
	

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
