package org.chy.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * 供前端快速查询的API配置类
 * @author  Chenghy
 * @date    2018年01月02日  10时28分10秒
 * @version 1.0
 */
@Configuration
@EnableSwagger2
public class APISwaggerConfig {
	
	@Bean
	public Docket createRestApi() {
		return new Docket(DocumentationType.SWAGGER_2)
				.apiInfo(apiInfo())
				.host("localhost")
				.select()
				.apis(RequestHandlerSelectors.basePackage("org.chy.controller"))
				.paths(PathSelectors.any())  
				.build();
	}
	
	private ApiInfo apiInfo() {
		return new ApiInfoBuilder()
				.title("api")
				.description("Chenghy---API")
				.termsOfServiceUrl("http://www.baidu.com")
				.contact(new Contact("chy", "localhost", "15228125636@163.com"))
				.version("1.0")
				.build();
	}
}

















