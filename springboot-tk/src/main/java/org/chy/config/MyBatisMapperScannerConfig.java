package org.chy.config;

import java.util.Properties;

import org.chy.utils.util.MyMapper;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


import tk.mybatis.spring.mapper.MapperScannerConfigurer;

/**
 * @author  Chenghy
 * @date    2017年12月20日  11时06分59秒
 * @version 1.0
 */
@Configuration
@AutoConfigureAfter(MyBatisAutoConfig.class)
public class MyBatisMapperScannerConfig {	
	
	@Bean
	public MapperScannerConfigurer mapperScannerConfigurer () {
		MapperScannerConfigurer mapperScannerConfigurer = new MapperScannerConfigurer();
		mapperScannerConfigurer.setSqlSessionFactoryBeanName("sqlSessionFactory");
		mapperScannerConfigurer.setBasePackage("org.chy.mapper");
		Properties properties = new Properties();
		properties.setProperty("mappers",MyMapper.class.getName());
		properties.setProperty("notEmpty", "false");
		properties.setProperty("IDENTITY", "MYSQL");
		mapperScannerConfigurer.setProperties(properties);
		return mapperScannerConfigurer;
	}

}


















