package org.chy.config;

import org.apache.ibatis.session.ExecutorType;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.core.io.Resource;

/**
 * @author Chenghy
 * @date 2017年12月20日 11时10分00秒
 * @version 1.0
 */
@ConfigurationProperties(prefix = MyBatisProperties.MYBATIS_PREFIX)
public class MyBatisProperties {

	public static final String MYBATIS_PREFIX = "mybatis";

	private String configPath;

	private Resource[] mapperLoacations;

	private String typeAliasesPackage;

	private String typeHandlersPackage;

	private Boolean checkConfigLocation = false;

	private ExecutorType executorType = ExecutorType.SIMPLE;

	public String getConfigPath() {
		return configPath;
	}

	public void setConfigPath(String configPath) {
		this.configPath = configPath;
	}

	public Resource[] getMapperLoacations() {
		return mapperLoacations;
	}

	public void setMapperLoacations(Resource[] mapperLoacations) {
		this.mapperLoacations = mapperLoacations;
	}

	public String getTypeAliasesPackage() {
		return typeAliasesPackage;
	}

	public void setTypeAliasesPackage(String typeAliasesPackage) {
		this.typeAliasesPackage = typeAliasesPackage;
	}

	public String getTypeHandlersPackage() {
		return typeHandlersPackage;
	}

	public void setTypeHandlersPackage(String typeHandlersPackage) {
		this.typeHandlersPackage = typeHandlersPackage;
	}

	public Boolean getCheckConfigLocation() {
		return checkConfigLocation;
	}

	public void setCheckConfigLocation(Boolean checkConfigLocation) {
		this.checkConfigLocation = checkConfigLocation;
	}

	public ExecutorType getExecutorType() {
		return executorType;
	}

	public void setExecutorType(ExecutorType executorType) {
		this.executorType = executorType;
	}

}
