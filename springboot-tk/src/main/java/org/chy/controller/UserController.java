package org.chy.controller;

import java.util.List;

import org.chy.entity.User;
import org.chy.service.UserService;
import org.chy.utils.result.ResultBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author  chenghy
 * @date    2018年01月07日 13时28分15秒
 * @Description 发上等愿,结中等缘,享下等福;向高处立,就平处坐,从宽处行.
 * @Package org.chy.controller
 */
@RestController
public class UserController {
	
	private @Autowired UserService userService;
	
	@GetMapping("getListUser")
	public ResultBean<List<User>> listUser(){
		return new ResultBean<List<User>>(userService.queryUserList());
	}
	
}
