package org.chy;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author  Chenghy
 * @date    2017年12月20日  09时48分00秒
 * @version 1.0
 */
@SpringBootApplication
@MapperScan(basePackages="org.chy.mapper")
public class TkApplication {

	public static void main(String[] args) {
		SpringApplication.run(TkApplication.class, args);
	}
	
}
