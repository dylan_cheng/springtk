package com.szkj.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.szkj.entity.Employee;
import com.szkj.entity.vo.AppointForMeVo;
import com.szkj.entity.vo.MeToInvitationVo;
import com.szkj.entity.vo.VisitAppointVo;
import com.szkj.service.EmployeeEntranceService;
import com.szkj.utils.Constants;
import com.szkj.utils.MD5Util;
import com.szkj.utils.WebUtils;
import com.szkj.utils.result.ResultBean;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**
 * 员工
 * @author  Chenghy
 * @date    2017年12月21日  01时57分12秒
 * @version 1.0
 */
@RestController
@Api(value = "员工操作" , description = "员工入口")
public class EmployeeController {
	
	private @Autowired EmployeeEntranceService employeeEntranceService;
	
	/**
	 *  员工入口[登录操作] 登录名和密码(登录名==姓名)
	 *  @param employee
	 *  @return
	 */
	@ApiOperation(value = "员工登录" , notes = "员工登录")
	@ApiImplicitParam(name="employee" , value = "员工对象", required = true, dataType="com.szkj.entity.Employee")
	@PostMapping("employeeEntrance")
	public ResultBean<String> employeeEntrance(@RequestBody Employee employee) {
		Employee currEmployee = employeeEntranceService.queryLoginEmployeeByPhone(employee.getLoginName());
		String currPassword = null;
		try {
			currPassword = MD5Util.md5Encode(employee.getPassword());
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (StringUtils.isEmpty(currEmployee))
			return new ResultBean<String>("该员工不存在!");
		else if (!currEmployee.getLoginName().equals(employee.getLoginName()))
			return new ResultBean<String>("登录名错误!");
		else if (!currEmployee.getPassword().equals(currPassword))
			return new ResultBean<String>("密码错误!");
		//将这个员工的信息保存到session中
		WebUtils.setSession(Constants.LOGING, currEmployee);
		return new ResultBean<String>("登录成功!");
	}
	
	/**
	 *  绑定员工信息(保存员信息到数据库中),绑定信息之后立马让该员工去做登录操作
	 *  @return
	 */
	@ApiOperation(value = "员工信息绑定" , notes = "保存员工信息")
	@ApiImplicitParam(name="employee" , value = "员工对象", required = true, dataType="com.szkj.entity.Employee")
	@PostMapping("saveEmployee")
	public ResultBean<Integer> saveEmployee(@RequestBody Employee employee) {
		return new ResultBean<Integer>(employeeEntranceService.saveEmployee(employee));
	} 
	
	/**
	 * 预约我的:
	 *   <1>:分页列表查询  =========> 状态:1:待审核、2:预约成功、3:已拒绝
	 * 		   返回: 查询Visiting(访客)的信息
	 *  		预约时间、预约人的姓名、预约人所在的公司名称、我给该访客设置的状态(该状态指的是预约是否成功)
	 *   <2>:预约我的[访客的详情信息]:
	 *   	   通过访客的id去查询该来访者的信息、被访者信息（其实这步可以省略的）、预约信息
	 *   	查看了详情后,会有允许、拒绝按钮的操作.
	 *     允许、拒绝: 修改其状态值[Visiting中的status].
	 *  @param start  页码
	 *  @param length 页大小
	 *  @return
	 */
	@ApiOperation(value="获取预约我的信息数据",notes="分页查询")
	@ApiImplicitParams({ 
		@ApiImplicitParam(name = "start", value = "页码", required = false, dataType = "Integer" ),
		@ApiImplicitParam(name = "length", value = "每页显示的行数", required = false, dataType = "Integer") 
	})
	@PostMapping("listVisitings")
	public ResultBean<Object> listVisitings(
		@RequestParam(required = false , defaultValue = "1", name="start") Integer start,
		@RequestParam(required = false , defaultValue = "3", name="length") Integer length) {
		PageHelper.startPage(start,length);
		//获取当前员工信息
		Employee employee = WebUtils.getSession(Constants.LOGING);
		if (StringUtils.isEmpty(employee)) {
			return new ResultBean<Object>("你尚未登录!");
		}
		List<AppointForMeVo> lVisitings = employeeEntranceService.queryAppointForMeVoList(employee.getId());
		PageInfo<AppointForMeVo> pages = new PageInfo<>(lVisitings);
		return new ResultBean<Object>(pages);
	}
	
	/**
	 *  通过访客id查询该访客的所有信息[通过sz_visiting_user_info表来查询]
	 *  经过修改,使用sz_visiting_user_info的id来查询该
	 *  @param id 访客id(Visiting--id)
	 *  @return 返回VisitAppointVo实体
	 */
	@PostMapping("getOnVisitAppointVoById")
	public ResultBean<VisitAppointVo> getOnVisitAppointVoById(String vid) {
		return new ResultBean<VisitAppointVo>(employeeEntranceService.findVisitAppointVoById(vid));
	}
	
	/**
	 *  被访者看到来访者后对其做的允许操作
	 *  @param status 状态:1:待审核、2:预约成功、3:已拒绝  ==========>此状态保存在sz_appoint_info中的status
	 *  @return 修改后的返回数
	 */
	@PostMapping("allowOperationVisitingStatus")
	public ResultBean<Integer> allowOperationVisitingStatus(String infoId) {
		return new ResultBean<Integer>(employeeEntranceService.updateVisitingAllowStatus(infoId));
	}
	
	/**
	 *  被访者看到来访者后对其做的拒绝操作
	 *  @param status 状态:1:待审核、2:预约成功、3:已拒绝  ==========>此状态保存在sz_appoint_info中的status
	 *  @return 修改后的返回数
	 */
	@PostMapping("refuseOperationVisitingStatus")
	public ResultBean<Integer> refuseOperationVisitingStatus(String infoId) {
		return new ResultBean<Integer>(employeeEntranceService.updateVisitingRefuseStatus(infoId));
	}
	
	/**
	 *  我的邀约----邀请贵宾(代贵宾填写)
	 *  @param visitAppointVo
	 *  @return
	 */
	@PostMapping("invitingVIP")
	public ResultBean<Integer> invitingVIP(@RequestBody VisitAppointVo visitAppointVo){
		return new ResultBean<Integer>(employeeEntranceService.insertInvitingVIP(visitAppointVo));
	}
	
	/**
	 *  我的邀约 --- 查询出我邀约的人
	 *  @return
	 */
	@PostMapping("meToInvitation")
	public ResultBean<Object> meToInvitation(
			@RequestParam(name = "start", defaultValue = "1", required = false) Integer start,
			@RequestParam(name = "length", defaultValue = "3", required = false) Integer length) {
		PageHelper.startPage(start, length);
		Employee employee = WebUtils.getSession(Constants.LOGING);
		if (StringUtils.isEmpty(employee)) {
			return new ResultBean<Object>("你尚未登录!");
		}
		List<MeToInvitationVo> lVisitings = employeeEntranceService.queryMeToInvitationVoList(employee.getId());
		PageInfo<MeToInvitationVo> pages = new PageInfo<>(lVisitings);
		return new ResultBean<Object>(pages);
	}
}









