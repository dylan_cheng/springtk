package com.szkj.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.szkj.entity.Employee;
import com.szkj.entity.Visiting;
import com.szkj.entity.vo.VisitAppointVo;
import com.szkj.service.EmployeeEntranceService;
import com.szkj.utils.Constants;
import com.szkj.utils.MD5Util;
import com.szkj.utils.WebUtils;
import com.szkj.utils.result.ResultBean;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**
 * 员工
 * @author  Chenghy
 * @date    2017年12月21日  01时57分12秒
 * @version 1.0
 */
@Controller
@Api(value = "员工操作" , description = "员工入口")
public class EmployeeController {
	
	private @Autowired EmployeeEntranceService employeeEntranceService;
	
	/**
	 *  员工入口[登录操作] 登录名和密码(登录名==姓名)
	 *  @param employee
	 *  @return
	 */
	@ApiOperation(value = "员工登录" , notes = "员工登录")
	@ApiImplicitParam(name="employee" , value = "员工对象", required = true, dataType="com.szkj.entity.Employee")
	@PostMapping("employeeEntrance")
	@ResponseBody
	public ResultBean<String> employeeEntrance(@RequestBody Employee employee) {
		Employee currEmployee = employeeEntranceService.loginEmployeeByPhone(employee.getName());
		String currPassword = null;
		try {
			currPassword = MD5Util.md5Encode(employee.getPassword());
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (StringUtils.isEmpty(currEmployee))
			return new ResultBean<String>("该员工不存在!");
		else if (!currEmployee.getName().equals(employee.getName()))
			return new ResultBean<String>("登录名错误!");
		else if (!currEmployee.getPassword().equals(currPassword))
			return new ResultBean<String>("密码错误!");
		//将这个员工的信息保存到session中
		WebUtils.setSession(Constants.LOGING, currEmployee);
		return new ResultBean<String>("登录成功!");
	}
	
	/**
	 *  绑定员工信息(保存员信息到数据库中),绑定信息之后立马让该员工去做登录操作
	 *  @return
	 */
	@ApiOperation(value = "员工信息绑定" , notes = "保存员工信息")
	@ApiImplicitParam(name="employee" , value = "员工对象", required = true, dataType="com.szkj.entity.Employee")
	@PostMapping("saveEmployee")
	@ResponseBody
	public ResultBean<Integer> saveEmployee(@RequestBody Employee employee) {
		return new ResultBean<Integer>(employeeEntranceService.saveEmployee(employee));
	} 
	
	/**
	 * 预约我的:
	 *   <1>:分页列表查询  =========> 状态:1:待审核、2:预约成功、3:已拒绝
	 * 		   返回: 查询Visiting(访客)的信息
	 *  		预约时间、预约人的姓名、预约人所在的公司名称、我给该访客设置的状态(该状态指的是预约是否成功)
	 *   <2>:预约我的[访客的详情信息]:
	 *   	   通过访客的id去查询该来访者的信息、被访者信息（其实这步可以省略的）、预约信息
	 *   	查看了详情后,会有允许、拒绝按钮的操作.
	 *     允许、拒绝: 修改其状态值[Visiting中的status].
	 *  @param start  页码
	 *  @param length 页大小
	 *  @return
	 */
	@ApiOperation(value="获取预约我的信息数据",notes="分页查询")
	@ApiImplicitParams({ 
		@ApiImplicitParam(name = "start", value = "页码", required = false, dataType = "Integer" ),
		@ApiImplicitParam(name = "length", value = "每页显示的行数", required = false, dataType = "Integer") 
	})
	@PostMapping("listVisitings")
	@ResponseBody
	public ResultBean<PageInfo<Visiting>> listVisitings(
		@RequestParam(required = false , defaultValue = "1", name="start") Integer start,
		@RequestParam(required = false , defaultValue = "3", name="length") Integer  length) {
		PageHelper.startPage(start, length);
		List<Visiting> lVisitings = employeeEntranceService.visitingsAll();
		PageInfo<Visiting> pages = new PageInfo<>(lVisitings);
		return new ResultBean<PageInfo<Visiting>>(pages);
	}
	
	/**
	 *  通过访客id查询该访客的所有信息[通过sz_visiting_user_info表来查询]
	 *  经过修改,使用sz_visiting_user_info的id来查询该
	 *  @param id 访客id(Visiting--id)
	 *  @return 返回VisitAppointVo实体
	 */
	@PostMapping("getOnVisitAppointVoById")
	@ResponseBody
	public ResultBean<VisitAppointVo> getOnVisitAppointVoById(String id) {
		return new ResultBean<VisitAppointVo>(employeeEntranceService.findVisitAppointVoById(id));
	}
	
	/**
	 *  被访者看到来访者后对其做的允许、拒绝等操作
	 *  @param status 状态:1:待审核、2:预约成功、3:已拒绝  ==========>此状态保存在sz_appoint_info中的status
	 *  @return 修改后的返回数
	 */
	@PostMapping("operationVisitingStatus")
	@ResponseBody
	public ResultBean<Integer> operationVisitingStatus(VisitAppointVo visitAppointVo) {
		return new ResultBean<Integer>(employeeEntranceService.updateVisitingStatus(visitAppointVo));
	}
	
}









